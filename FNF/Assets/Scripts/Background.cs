﻿using UnityEngine;
using System.Collections;

public class Background : MonoBehaviour {

	public Vector2 velocity = new Vector2 (-1, 0);
	public GameObject three;
	public GameObject two;
	public GameObject one;
	public GameObject go;
	
	void Start ()
	{
		StartCoroutine ("Wait");
	}

	IEnumerator Wait()
	{
		yield return new WaitForSeconds (1);
		three.SetActive (false);
		two.SetActive (true);
		yield return new WaitForSeconds (1);
		two.SetActive (false);
		one.SetActive (true);
		yield return new WaitForSeconds (1);
		one.SetActive (false);
		go.SetActive (true);
		yield return new WaitForSeconds (1);
		go.SetActive (false);
		GetComponent<Rigidbody2D>().velocity = velocity;
	}
	
	void OnBecameInvisible ()
	{
		Destroy (gameObject);
	}
}
