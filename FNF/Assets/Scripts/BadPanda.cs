﻿using UnityEngine;
using System.Collections;

public class BadPanda : MonoBehaviour {
		
	public Vector2 velocity = new Vector2 (-3, 0);
	private float startY = 0;
	private bool iAmLast = false;
	
	void Start ()
	{
		GetComponent<Rigidbody2D>().velocity = velocity;
		startY = Random.Range (-4.5f, 4.5f);
		transform.position = new Vector3 (
			transform.position.x,
			startY,
			transform.position.z);
	}

	void Update()
	{
		transform.position = new Vector3 (
			transform.position.x,
			startY + Mathf.Sin (transform.position.x),
			transform.position.z);
	}

	void OnBecameInvisible ()
	{
		if(iAmLast)
			Application.LoadLevel ("Menu");
		Destroy (gameObject);
	}

	public void youAreLast() {
		iAmLast = true;
	}
}
