﻿using UnityEngine;
using System.Collections;

public class Cloud1 : MonoBehaviour {
	
	Vector3 velocity;
	
	void Start ()
	{
		velocity = new Vector3 (-0.05f, 0f, 0f);
		transform.position = new Vector3 (
			transform.position.x,
			Random.Range (-5, 5),
			transform.position.z);
	}
	
	void Update ()
	{
		transform.position = transform.position + velocity;
	}
	
	void OnBecameInvisible ()
	{
		Destroy (gameObject);
	}
}
