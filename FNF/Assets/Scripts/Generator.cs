﻿using UnityEngine;
using System.Collections;

public class Generator : MonoBehaviour {
	
	public GameObject cloud1;
	public GameObject cloud2;
	public GameObject cloud3;
	public GameObject badpanda;
	public UIManager uimanager;
	private int badPandaCounter = 0;
	
	void Start ()
	{
		InvokeRepeating ("CreateCloud", 0f, 3.5f);
		InvokeRepeating ("CreateBadPanda", 1f, 1.7f);
	}
	
	void CreateCloud () {
		int r = Random.Range (0, 4);
		if (r == 0) {
			Instantiate (cloud1);
		} else if (r == 1) {
			Instantiate (cloud2);
		} else if (r == 2) {
			Instantiate (cloud3);
		}
	}

	void CreateBadPanda () {
		GameObject badPanda = Instantiate (badpanda);
		badPandaCounter++;
		if (badPandaCounter >= 30) {
			StopCreatingPandas (badPanda);
		}
	}

	void StopCreatingPandas(GameObject theLastBadPanda)
	{
		CancelInvoke ("CreateBadPanda");
		uimanager.Resurrect ();
	}
}
