﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

	enum State {START,WALKING,JUMP,JUMPING,DIE,DYING,DEAD};
	
	public GameObject score;
	public GameObject terrain;
	public GameObject background;
	public GameObject backgroundNear;
	Vector2           jumpForce = new Vector2 (0, 400);
	State             state     = State.START;
	long              timeout;
	
	void FixedUpdate () //Fixed Update for phyisics stuff
	{
		Rigidbody2D panda = GetComponent<Rigidbody2D> ();
		Vector2 screenPosition = Camera.main.WorldToScreenPoint (transform.position);

		switch (state) {
		case State.START:
		case State.JUMP:
		case State.JUMPING:
		case State.WALKING:
			panda.position = new Vector2(-3, panda.position.y);
			break;
		default:
			break;
		}

		switch (state) {
		case State.START:
			state = State.WALKING;
			break;
		case State.WALKING:

			//jump
			if (Input.GetKeyDown("space") || (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)) {
				state = State.JUMP;
			}

			if (screenPosition.y < 0)		                                                        
				Die();

			break;
		case State.JUMP:
			Animator anim = GetComponent<Animator>();
			anim.CrossFade("Jumping", 0f);

			panda.velocity = Vector2.zero;
			panda.AddForce (jumpForce);
			state = State.JUMPING;
			break;

		case State.JUMPING:
			if (screenPosition.y < 0)		                                                        
				Die();
			break;

		case State.DIE:
			GetComponents<AudioSource>()[0].Play ();
			timeout = System.DateTime.Now.Ticks + 20000000;
			state = State.DYING;
			break;
		case State.DYING:
			if(System.DateTime.Now.Ticks > timeout)
				state = State.DEAD;
			break;
		case State.DEAD:
			//Application.LoadLevel (Application.loadedLevel);
			Application.LoadLevel ("Resurrection Mode");
			break;
		}
	}
	
	void OnCollisionEnter2D (Collision2D other)
	{
		Debug.Log (other.gameObject.name);
		if (other.gameObject.name.ToLower().StartsWith ("terrain")) {
			if (state == State.JUMPING) {
				Animator anim = GetComponent<Animator> ();
				anim.CrossFade ("Walking", 0f);
				
				state = State.WALKING;
			}

		} else {
			Die();
		}
	}

	void Die() {
		Animator anim = GetComponent<Animator> ();
		anim.CrossFade ("Die", 0f);
		state = State.DIE;
		terrain.GetComponent<Rigidbody2D>().velocity = new Vector2(0,0);
		background.GetComponent<Rigidbody2D>().velocity = new Vector2(0,0);
		backgroundNear.GetComponent<Rigidbody2D>().velocity = new Vector2(0,0);
	}
}