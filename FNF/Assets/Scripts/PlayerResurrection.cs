﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerResurrection : MonoBehaviour {
	
	public Vector2 jumpForce = new Vector2(0, 300);
	public UIManager uimanager;
	public GameObject gameOver;
	bool isDead;
	
	// Update is called once per frame
	void Update () {
		if ((Input.GetKeyDown ("space") || (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)) && !isDead) {
			GetComponent<Rigidbody2D>().velocity = Vector2.zero;
			GetComponent<Rigidbody2D>().AddForce (jumpForce);
		}
		
		Vector2 screenPosition = Camera.main.WorldToScreenPoint (transform.position);
		
		if (screenPosition.y > Screen.height || screenPosition.y < 0)
			Die ();
	}
	
	void OnCollisionEnter2D (Collision2D collision) {
		//collision.collider.GetComponent<Rigidbody2D> ().velocity = Vector2.zero;
		Die ();
	}
	
	void Die () {
		if (!(isDead)) {
			isDead = true;
			GetComponents<AudioSource>()[0].Play ();
			GetComponents<AudioSource>()[1].Play ();
			gameOver.SetActive(true);
			StartCoroutine("GameOver");
		}
	}

	IEnumerator GameOver(){
		yield return new WaitForSeconds(3);
		Application.LoadLevel ("Menu");
	}
}
