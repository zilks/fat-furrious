﻿using UnityEngine;
using System.Collections;

public class Terrain : MonoBehaviour {

	public Vector2 velocity = new Vector2 (-2, 0);
	
	void Start ()
	{
		StartCoroutine ("Wait");
	}
	
	IEnumerator Wait()
	{
		yield return new WaitForSeconds (4);
		GetComponent<Rigidbody2D>().velocity = velocity;
	}
	
	void OnBecameInvisible ()
	{
		Destroy (gameObject);
	}
}
