﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

	public GameObject settingsPanel;
	
	public void StartGame () {
		Application.LoadLevel ("Level01");
	}	

	public void StopGame () {
		Application.LoadLevel ("Menu");
	}	

	public void Settings() {
		settingsPanel.SetActive( true);
	}

	public void SettingsClose() {
		settingsPanel.SetActive ( false);
	}

	public void Resurrect() {
		Application.LoadLevel ("Level01");
	}
}
